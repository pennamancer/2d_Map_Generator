/* 2D Map Generator written by Ryan Martell
 *
 * TODO:
 * Allow specification of map dimensions (row x col) when generating
 * Allow option to disable optional paths (side paths)
 * Prevent optional paths from connecting to the final room
 * Add option to allow influencing the max length of optional paths
 *
 */

import java.util.LinkedList;
import java.util.Random;

public class Map {
    //Instance variables
    //Map legend - Change these to change the look of the map
    private String s = "S"; //Start
    private String e = "F"; //End
    private String r = "+"; //Normal Room
    private String o = "-"; //Nothing Here


    //Current map - Text
    private String grid[][];

    //Current map - Rooms
    private Room roomGrid[][];


    //Constructor Method
    public Map(int rows, int cols) {
        this.grid = new String[rows][cols];
        //Create a map of the specified size
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                if(i == 0 && j == 0) {
                    grid[i][j] = s;
                }
                else if(i == rows-1 && j == cols-1) {
                    grid[i][j] = e;
                }
                else {
                    grid[i][j] = o;
                }
            }
        }
        generateMap();
    }


    //Path - A linked list representing the path through the grid. Used for generation.
    private LinkedList<int[]> path = new LinkedList<>();

    //Current coordinate in generation
    private int currRow = 0;
    private int currCol = 0;

    //Random number generator
    private Random rand = new Random();

    //Has finish been reached
    private boolean finishFound = false;

    //Number of failed generations for current room. Resets every time a new room is created.
    private int failedGen = 0;

    //Number of failed generations for current path attempt. Resets after x total failures.
    private int totalFailed = 0;

    //Debug fail count (Real total failures, never resets).
    private int debugFail = 0;



    //Get Methods
    public Room[][] getRoomGrid() {
        return roomGrid;
    }
    public String[][] getGrid() {
        return grid;
    }


    //Set Methods
    public void setRoomGrid(Room[][] roomGrid) {
        this.roomGrid = roomGrid;
    }
    public void setGrid(String[][] grid) {
        this.grid = grid;
    }


    //Fill grid with a new map
    public void generateMap() {
        //Set first point
        int[] first = {0,0};
        path.add(first);

        //While finish has not been reached
        while (!finishFound) {
            //printMap(); debug - Print map after every path tile generation

            //If a valid room isn't found x times in a row. I.E. the generator is stuck
            if (failedGen >= 10) {

                //Back up one spot and go a different direction
                grid[currRow][currCol] = o;
                currRow = path.get(1)[0];
                currCol = path.get(1)[1];
                path.removeFirst();
                // System.out.println("Reset"); Debug - Label every time the generator backs up

            }

            //If the path isn't completed after x failures. I.E. the generator is really stuck
            //The smaller the allowed number of total failures, the greater the chance
            //the map will be linear. Must increase as map size increases.
            else if (totalFailed > 1000) {
                //Remove entire path and start over
                for (int i = 0; i < path.size() - 1; i++) {
                    grid[currRow][currCol] = o;
                    currRow = path.get(1)[0];
                    currCol = path.get(1)[1];
                    path.removeFirst();
                }
                totalFailed = 0;
                // System.out.println("TOTAL RESET"); Debug - Label every time the path generator resets
            }

            //Generate new step
            generateDirection();
        }

        //Print out the coordinates of each point in the path
        // for (int point[] : path) {
        //    System.out.print("row: " + point[0] + " col: " + point[1] + "\n");
        // }


        //Generate side paths
        generateOptional();


        //Replicate the text grid in the room grid array
        roomGrid = new Room[grid.length][grid[0].length];
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[i].length; j++) {
                if(grid[i][j].equals(s) || grid[i][j].equals(r) || grid[i][j].equals(e)) {
                    roomGrid[i][j] = new Room(new int[] {j, i}, roomGrid); //(x, y)
                }
            }
        }
    }


    //Generate a direction to move
    public void generateDirection() {
        //Randomly generate a direction, horizontal or vertical
        int HoV = rand.nextInt(2);

        //If Horizontal
        if (HoV == 0) {

            //Randomly chose west or east
            int WoE = (int)(rand.nextInt(2));

            //West
            if (WoE == 0)
                validateStep(currRow, currCol-1);

                //East
            else
                validateStep(currRow, currCol+1);
        }

        //If vertical
        else {

            //Randomly chose north or south
            int NoS = (int)(rand.nextInt(2));

            //North
            if (NoS == 0)
                validateStep(currRow-1, currCol);

                //South
            else
                validateStep(currRow+1, currCol);
        }
    }


    //Check if direction generated is a valid new tile
    public void validateStep(int currRow, int currCol) {
        //Check if direction is off the grid, if so try again
        if (isOffGrid(currRow, currCol)) { failedGen++; totalFailed++; debugFail++; }

        //If not
        else {

            //Check to make sure direction does not have more than one neighbor cell (The one you came from)
            if (neighborCheck(currRow, currCol) > 1) { failedGen++; totalFailed++; debugFail++; }

            //If not
            else {

                //Make sure the space isn't occupied by another room (rare case)
                if (grid[currRow][currCol] == r || grid[currRow][currCol] == s) { failedGen++; totalFailed++; debugFail++; }

                //If not
                else {
                    //This is a valid room or the finish
                    this.currRow = currRow;
                    this.currCol = currCol;

                    //Check if this is the finish
                    if (grid[currRow][currCol] == e) {
                        finishFound = true;
                        failedGen = 0;
                        path.addFirst(new int[]{currRow, currCol});
                    }

                    //Else this becomes the next room in the chain
                    else {
                        grid[currRow][currCol] = r;
                        failedGen = 0;
                        path.addFirst(new int[]{currRow, currCol});
                    }
                }
            }
        }
    }


    //Generate side paths
    public void generateOptional() {
        //For each room, there is a 1 in x-1 chance that a side path will branch off of it
        for (int i = 0; i < path.size(); i++) {
            if (rand.nextInt(5) == 4) {
                currRow = path.get(i)[0];
                currCol = path.get(i)[1];

                //The generator gets x tries at generating valid rooms that make up the optional path, after which it stops
                while (failedGen <= 25) {
                    // System.out.println("Offshoot Attempt"); Debug - print and label grids for each offshoot attempt
                    // printMap();

                    generateDirection();
                }

                failedGen = 0;
            }
        }
    }


    //Check if a given set of coordinates is outside the bounds of a given map grid
    public boolean isOffGrid(int row, int col) {
        if (row < 0 || row >= grid.length || col < 0 || col >= grid[0].length)
            return true;
        else
            return false;
    }


    //Check the number of "neighbors" a given indicy has directly to its North, South, East and West
    public int neighborCheck(int x, int y) {
        int neighborCount = 0;
        if (!isOffGrid(x-1, y) && (grid[x-1][y] == r || grid[x-1][y] == s))
            neighborCount++;
        if (!isOffGrid(x+1, y) && (grid[x+1][y] == r || grid[x+1][y] == s))
            neighborCount++;
        if (!isOffGrid(x, y+1) && (grid[x][y+1] == r || grid[x][y+1] == s))
            neighborCount++;
        if (!isOffGrid(x, y-1) && (grid[x][y-1] == r || grid[x][y-1] == s))
            neighborCount++;

        return neighborCount;
    }


    //Print out the map grid w/ proper formatting
    public void printMap() {
        String output = "";
        for (int i = 0; i < grid.length; i++) {
            for (int u = 0; u < grid[i].length; u++) {
                output += grid[i][u];
            }
            output += "\n";
        }

        System.out.println(output);
    }
}