public class Door {
    //Instance variables
    private boolean open;
    private boolean locked;
    private Room destination;


    //Constructor method
    public Door(Room destination) {
        this.open = false;
        this.locked = true;
        this.destination = destination;
    }


    //Set methods
    public void setOpen(boolean open) {
        this.open = open;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setDestination(Room destination) {
        this.destination = destination;
    }


    //Get methods
    public boolean getOpen() {
        return open;
    }

    public boolean getLocked() {
        return locked;
    }

    public Room getDestination() {
        return destination;
    }


    //toString method
    public String toString() {
        return "Door open: " + open + "\nDoor locked: " + locked + "\nDestination: " + destination;
    }
}
